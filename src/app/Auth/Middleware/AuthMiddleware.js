import admin from '../../../config/firebase';

const verifyAuthentication = (req, res, next) => {
    if (req.session.user) {
        return next();
    }
    return res.redirect('/login');
};

const verifyNotAuthtication = (req, res, next) => {
    if (!req.session.user) {
        return next();
    }

    return res.redirect('/');
};

const saveSession = (req, res, next) => {
    req.session.user = req.body;
    req.session.save();
    return next();
};

const checkIDToken = (req, res, next) => {
    admin.auth().verifyIdToken(req.body.idToken)
    .then(() => next()).catch(() => {
        res.json('error');
    });
};

export default {
    checkIDToken,
    verifyAuthentication,
    verifyNotAuthtication,
    saveSession,
};
