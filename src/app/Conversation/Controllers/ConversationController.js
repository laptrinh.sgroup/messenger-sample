import BaseController from '../../../infrastructure/Controllers/BaseController';
import Service from '../Services/ConversationService';
import { currentId } from 'async_hooks';
import { LakeFormation } from 'aws-sdk';

class ConversationController extends BaseController {
  constructor() {
    super();
    this.service = Service.getService();
  }

  groupAddMethod(req, res) {
    const data = req.body;
    data.users_ID = JSON.parse(data.users_ID);
    data.users_objId = JSON.parse(data.users_objId);
    this.service.groupAddService(data);
    res.json('successful');
  }

  async getObjId(req, res) {
    const data = req.body;
    data.users = JSON.parse(data.users);
    const objId = await this.service.getObjId(data);
    return res.json(objId);
  }

  async listConversation(req, res) {
    const ID = req.session.user.id;
    const conversations = await this.service.listConversation(ID);
    return res.json({ conversations, ID });
  }
}

export default ConversationController;
