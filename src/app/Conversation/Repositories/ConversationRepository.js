import BaseRepository from '../../../infrastructure/Repositories/BaseRepository';

import model from '../../../database/models/index';

class ConversationRepository extends BaseRepository {
  static repository;

  static getRepository() {
    if (!this.repository) {
      this.repository = new this();
    }
    return this.repository;
  }

  getTableName() {
    return 'conversations';
  }

  groupAdd(data) {
    model.Conversation.create({
      groupName: data.groupName,
      member: data.users_ID,
      description: data.description,
      userIds: data.users_objId,
    });
  }

  async getObjId(data) {
    const query = await model.User.find({
      id: data.users[0],
    });
    return query;
  }

  async yourObjId(ID) {
    const query = await model.User.find({
      id: ID,
    });
    return query;
  }

  listConversation(objId) {
    return model.Conversation
    .find({ userIds: { $elemMatch: { $eq: objId[0]._id } } })
    .select('groupName description -_id')
    .populate('userIds', 'name avatar id -_id');
  }
}

export default ConversationRepository;
