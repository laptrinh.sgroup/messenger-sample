import express from 'express';
import ConversationController from '../Controllers/ConversationController';
import FriendController from '../../Friends/Controllers/FriendController';
import middleware from '../../Auth/Middleware/AuthMiddleware';

const router = express.Router();
// Declare Object
const conversationController = new ConversationController();

router.post('/group-add', middleware.verifyAuthentication, conversationController.callMethod('groupAddMethod'));
router.post('/get-objId', middleware.verifyAuthentication, conversationController.callMethod('getObjId'));
router.post('/list-conversation', middleware.verifyAuthentication, conversationController.callMethod('listConversation'));
export default router;
