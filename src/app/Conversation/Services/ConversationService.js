import ConversationRepository from '../Repositories/ConversationRepository';

class ConversationService {
  static service;

  static io;

  constructor() {
    this.repository = ConversationRepository.getRepository();
  }

  static getService() {
    if (!this.service) {
      this.service = new this();
    }
    return this.service;
  }

  async groupAddService(data) {
    await this.repository.groupAdd(data);
  }

  async getObjId(data) {
    const objId = await this.repository.getObjId(data);
    return objId;
  }

  async listConversation(ID) {
    const objId = await this.repository.yourObjId(ID);
    const list = this.repository.listConversation(objId);
    return list;
  }

  async postData(data) {
    const { conversationId } = data;
    ConversationService.io.to(conversationId).emit('newMessage', data);
  }
}

export default ConversationService;
