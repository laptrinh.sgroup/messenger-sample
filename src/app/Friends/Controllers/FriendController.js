import BaseController from '../../../infrastructure/Controllers/BaseController';
import Service from '../Services/FriendService';

class FriendController extends BaseController {
  constructor() {
    super();
    this.service = Service.getService();
  }

  addFriend(req, res) {
    const userID = req.session.user.id;
    const data = req.body;
    this.service.addFriend(data, userID, res);
  }

  async listRequest(req, res) {
    const userID = req.session.user.id;
    const requests = await this.service.listRequest(userID);
    return res.render('app/conversation/index', { requests });
  }

  acceptFriend(req, res) {
    const data = req.body;
    data.userID = req.session.user.id;
    this.service.acceptFriend(data);
  }

  denyFriend(req, res) {
    const data = req.body;
    data.userID = req.session.user.id;
    this.service.denyFriend(data);
  }
}

export default FriendController;
