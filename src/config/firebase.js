import admin from 'firebase-admin';
import serviceAccount from '../../serviceAccount';

const firebase = admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: 'https://mess-demo.firebaseio.com',
});

export default firebase;
