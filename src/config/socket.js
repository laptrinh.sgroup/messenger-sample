import socket from 'socket.io';
import ConversasionService from '../app/Conversation/Services/ConversationService';

export default function (server) {
  const io = socket(server);

  ConversasionService.io = io.of('/conversations').on('connection', (client) => {
    client.on('join', (id) => {
      client.join(id);
    });
  });
}
