import mongoose from 'mongoose';

const schema = new mongoose.Schema({
  id: Number,
  name: String,
  avatar: String,
}, {
  timestamps: true,
});
export default mongoose.model('users', schema);
