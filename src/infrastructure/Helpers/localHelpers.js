export default function (res) {
    res.locals.firebaseApiKey = process.env.FIREBASE_API_KEY;
    res.locals.firebaseProjectId = process.env.FIREBASE_PROJECT_ID;
    res.locals.firebaseSenderId = process.env.FIREBASE_SENDER_ID;
    res.locals.firebaseAppId = process.env.FIREBASE_APP_ID;
}
