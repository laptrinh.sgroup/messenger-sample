$(document).ready(function (){
    window.recaptchaVerifier = new firebase.auth.RecaptchaVerifier('recaptcha-container');
    
    $('#login-phone').submit(function (event) {
        event.preventDefault();
        let phoneNumber = $('input[name="phoneNumber"]').val();
        phoneNumber = '+84' + phoneNumber.substring(1,phoneNumber.length);
        const appVerifier = window.recaptchaVerifier;

      firebase.auth().signInWithPhoneNumber(phoneNumber, appVerifier)
      .then(function (confirmationResult) {
        window.confirmationResult = confirmationResult;
        $('#login-step1').css('display','none');
        $('#login-step2').css('display','block')

        $('#verify-phone-login').submit(function (event) {
          event.preventDefault();
          const codeUser = $('input[name="codeVerify"]').val()
          confirmationResult.confirm(codeUser).then(function (result) {
            const user = result.user;
            firebase.auth().currentUser.getIdToken(true).then(function(idToken) {
              const userInfo = {
                phoneNumber,
                idToken
              };  
            
              $.ajax({
                type:'POST',
                url:'/login-sms',
                data: userInfo,
                success : function(data) {
                  if(data == 'error') {
                    window.alert("Verify ID token fail!");
                  }
                  if(data == 'successful') {
                    window.alert("Welcome!");
                    window.location.replace('/');
                  }
                }
              });
            })
          }).catch(function (error) {  
            console.log(error);
          });
        })
      }).catch(function (error) {
        console.log(error);
      });
    });

    $('#register-phone').submit(function (event) {
      event.preventDefault();
      let phoneNumber = $('input[name="phoneNumber"]').val();
      phoneNumber = '+84' + phoneNumber.substring(1,phoneNumber.length);
      const appVerifier = window.recaptchaVerifier;
      const firstName = $('input[name="firstName"]').val(); 
      const lastName = $('input[name="lastName"]').val();
      
      firebase.auth().signInWithPhoneNumber(phoneNumber, appVerifier)
      .then(function (confirmationResult) {
        window.confirmationResult = confirmationResult;
        $('#register-step1').css('display','none');
        $('#register-step2').css('display','block')

        $('#verify-phone-register').submit(function (event) {
          event.preventDefault();
          const codeUser = $('input[name="codeVerify"]').val()
          confirmationResult.confirm(codeUser).then(function (result) { // compare codeUSer and code firebase
            // User signed in successfully.
            const user = result.user;
            const userInfo = {
                firstName,
                lastName,
                phoneNumber
            };
            
            $.ajax ({
              type:'POST',
              url:'/register-sms',
              data: userInfo,
              success: function() {
                window.alert("Welcome!");
                window.location.replace('/');
              }
            });
          }).catch(function (error) {
            console.log(error);
            
          });
        })
      }).catch(function (error) {
          console.log('error', error);
      });
    });

    $('#register-email').submit(function (event) {
      event.preventDefault(); 
      const email = $('input[name="email"]').val(); 
      const password = $('input[name="password"]').val();
      const firstName = $('input[name="firstName"]').val(); 
      const lastName = $('input[name="lastName"]').val();
      //firebase create
      firebase.auth().createUserWithEmailAndPassword(email, password)
      .then(function(result) {
        var user = firebase.auth().currentUser; 
        const userInfo = {
          firstName,
          lastName,
          email,
          password
        }

        //user exist => send email verification 
        if(user) {
          user.sendEmailVerification().then(function() {
            $.ajax ({
              type:'POST',
              url:'/register-email',
              data: userInfo,
              //Success neu tra ve file res.json
              success : function() {
                window.alert("Register successful! Verify email before login!");
                window.location.replace("/login-email");
              }
            })
          }) 
        }
        else {
          window.alert('ERROR!')
        } 
      })
    });

    $('#signIn-email').submit(function (event) {
      event.preventDefault();
      const email = $('input[name="email"]').val(); 
      const password = $('input[name="password"]').val();

      firebase.auth().signInWithEmailAndPassword(email, password)
      .then(function(result) {
        const user = firebase.auth().currentUser; 
        if(user.emailVerified) {
          firebase.auth().currentUser.getIdToken(true).then(function(idToken) {
            const userInfo = {
              email,
              password,
              idToken,
            };  
          
            $.ajax({
              type:'POST',
              url:'/login-email',
              data: userInfo,
              success : function(data) {
                  if(data == 'error') {
                    window.location.replace('/login-email');
                    window.alert('Wrong ID token!');
                  }
                  if(data == 'successful') {
                    window.location.replace('/');
                    window.alert('Welcome!');
                  }
              }
            });
          });
        }
        else { 
          window.alert("Login email and verify!");
        }
      }).catch(function(error) {
          console.log(error.message);
      });
    });
});

$('#logout').on('show.bs.modal', function () {
  $('#settingModal').modal('hide');
})

$(document).ready(function() {
  $('#users').keyup(function(event) {
      event.preventDefault();
      const fullname = $('input[name="q"]').val();
      $('#member option').remove();

      $.ajax({
        type: 'POST',
        url: '/search-user',
        data: {
          fullname,
        },
        success: function(data) {
          data.forEach(function(dt) {
            let check = 0;
            $('.user-id').each(function() {
              if($(this).data('user-id') == dt.id) {
                check =1;
              }
            });
            if(check == 0) {
              let html = "<option data-user-id = "+ dt.id + " value= '" + dt.fullName + "' class = 'memberOption'>"; 
            $('#member').append(html);
            }
          });
        }
      })

  });

  $('.addGroupUser').click(function(event) {
    event.preventDefault();
    let user = new Array;
    let objId = new Array;
    user.push($("#member option[value='" + $('input[name="q"]').val() + "']").data('user-id'));  
    let users =  JSON.stringify(user);
    $.ajax({
      type:'POST',
      url:'/get-objId',
      data: {
        users,
      },
      success: function(data) {
        let check = 0;
        $('.user-id').each(function() {
          if($(this).data('user-id') == data[0].id) {
            check =1;
          }
        });
        if(check == 0) {
          let html = "<figure class='avatar user-id' data-user-id="+ data[0].id +" data-obj-id=" + data[0]._id +"><span class='avatar-title bg-success rounded-circle'>E</span></figure>"; 
        $('.avatar-group').append(html);
        }
        else {
          alert('Exist in group!');
        }
      },
    });
  });

  $('#createGroup').submit(function(event) {
    event.preventDefault();
    const groupName = $('input[name="groupName"]').val();
    const description = $('textarea[name="description"]').val();
    let users_ID = new Array;
    let users_objId = new Array;
    $('.user-id').each(function() {
      users_ID.push($(this).data('user-id'));
      users_objId.push($(this).data('obj-id'));
    });
    users_ID =  JSON.stringify(users_ID);
    users_objId = JSON.stringify(users_objId);
    $.ajax({
        type:'POST',
        url:'/group-add',
        data: {
          users_ID,
          users_objId,
          groupName,
          description,
        },
        success: function(data) {
            $('#newGroup').modal('hide');
        },
    });
  });

  $('.friends').click(function(event) {
    event.preventDefault();
    $.ajax({
      type:'POST',
      url:'/list-conversation',
      success: function(data) {
        console.log(data.conversations[0].userIds.length);
        
          data.conversations.forEach(function(dt) {
            if(data.conversations[0].userIds.length == 2) {
              dt.userIds.forEach(function(ob) {
                if(data.ID != ob.id) {
                  let html = "<li class='list-group-item'><div><figure class='avatar'><img class='rounded-circle' src='dist/images/women_avatar5.jpg'></figure></div><div class='users-list-body'><h5>" + ob.name + "</h5><p>Lorem ipsum dolor sitsdc sdcsdc sdcsdcs</p><div class='users-list-action action-toggle'><div class='dropdown'><a data-toggle='dropdown' href='#'><i class='ti-more'></i></a><div class='dropdown-menu dropdown-menu-right'><a class='dropdown-item' href='#'>Open</a><a class='dropdown-item active' href='#' data-navigation-target='contact-information'>Profile</a><a class='dropdown-item' href='#'>Add to archive</a><a class='dropdown-item' href='#'>Delete</a></div></div></div></div></li>";
                  $('.list-group-flush').append(html);
                }
              })
            }
            else {
              let html = "<li class='list-group-item'><div><figure class='avatar'><img class='rounded-circle' src='dist/images/women_avatar5.jpg'></figure></div><div class='users-list-body'><h5>" + dt.groupName + "</h5><p>Lorem ipsum dolor sitsdc sdcsdc sdcsdcs</p><div class='users-list-action action-toggle'><div class='dropdown'><a data-toggle='dropdown' href='#'><i class='ti-more'></i></a><div class='dropdown-menu dropdown-menu-right'><a class='dropdown-item' href='#'>Open</a><a class='dropdown-item active' href='#' data-navigation-target='contact-information'>Profile</a><a class='dropdown-item' href='#'>Add to archive</a><a class='dropdown-item' href='#'>Delete</a></div></div></div></div></li>";
              $('.list-group-flush').append(html);
            }
          })
      },
  });
  })
});


