$(document).ready(function () {
    $('#editInfo').submit(function (event) {
        event.preventDefault(); 
        const fullName = $('input[name="fullName"]').val(); 
        const avatar = $('input[name="avatar"]').val(); 
        const city = $('input[name="city"]').val(); 
        let phoneNumber = $('input[name="phoneNumber"]').val(); 
        phoneNumber = '+84' + phoneNumber.substring(1,phoneNumber.length);
        const email = $('input[name="email"]').val(); 
        const description = $('input[name="description"]').val(); 
        const userInfo = {
            fullName,
            avatar,
            city,
            phoneNumber,
            email,
            description
        }
        $.ajax({
            type:'POST',
            url:'/editUser',
            data: userInfo,
            success : function() {
                alert("Successful!");
            }
        })
    });

    $('#addFriend').submit(function (event) {
        event.preventDefault(); 
        const info = $('input[name="info"]').val(); 
        const userInfo = {
            info,
        }
        $.ajax({
            type:'POST',
            url:'/add-friend',
            data: userInfo,
            success : function(data) {
                if(data == 'fail') {
                    window.alert("Email/Phone Number is invalid!");
                }
                if(data == 'successful') {
                    window.alert("Sent friend request!")
                }
            }
        });
    });

    $('.accept-request').click(function (event) {
        event.preventDefault();
        const friendID = $('.accept-request').attr('data-profile');
        $.ajax({
            type:'POST',
            url:'/accept-friend',
            data: {friendID},
        });
    })

    $('.deny-request').click(function (event) {
        event.preventDefault();
        const friendID = $('.deny-request').attr('data-profile');
        $.ajax({
            type:'POST',
            url:'/deny-friend',
            data: {friendID},
        });
    })
});