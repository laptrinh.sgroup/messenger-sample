import express from 'express';
import authRouter from '../app/Auth/Routes/routes';
import middleware from '../app/Auth/Middleware/AuthMiddleware';
// import FriendsController from '../app/Friends/Controllers/FriendController';
import conversationRouter from '../app/Conversation/Routes/routes';

// const friendsController = new FriendsController();

const router = express.Router();

router.use(authRouter);
router.use(conversationRouter);

router.get('/', middleware.verifyAuthentication, (req, res) => res.redirect('/conversations'));

router.get('/conversations', middleware.verifyAuthentication, (req, res) => res.render('app/conversation/index'));

export default router;
